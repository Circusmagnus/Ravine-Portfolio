package lukasz.wojtach.ravineportfolio.service;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import de.greenrobot.event.EventBus;
import lukasz.wojtach.ravineportfolio.model.AppDetails;
import lukasz.wojtach.ravineportfolio.model.AppEntry;
import lukasz.wojtach.ravineportfolio.events.AppDetailsReceivedEvent;
import lukasz.wojtach.ravineportfolio.events.AppListReceivedEvent;
import lukasz.wojtach.ravineportfolio.events.JsonNotFoundEvent;


public class JSONService {

    private static final String TAG = JSONService.class.getSimpleName();
    private static JSONService jsonServiceInstance;
    private RequestQueue queue;

    private JSONService(RequestQueue queue){
        this.queue = queue;
    }

    public static JSONService getInstance (RequestQueue queue){
        if(jsonServiceInstance == null){
            jsonServiceInstance = new JSONService(queue);
        }
        return jsonServiceInstance;
    }

    // gets a List of App Entries
    public void requestAppList(String url){

        // Request a JSon object response from the provided URL.
        JsonObjectRequest appListRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // Use volley to populate JSONArray jAppList with list of applications
                        JSONArray jAppList = response.optJSONObject("data").optJSONArray
                                ("portfolio");

                        // transform JSONArray into Array of AppEntry Objects
                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<Collection<AppEntry>>(){}.getType();
                            List<AppEntry> appEntries = gson.fromJson(String.valueOf(jAppList),
                                    collectionType);

                       // notify subscribers, that new List is ready
                        EventBus.getDefault().post(new AppListReceivedEvent(appEntries));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "json download error");
                EventBus.getDefault().post(new JsonNotFoundEvent());
            }
        });

        // increase request timeout
        int socketTimeout = 30000;//30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        appListRequest.setRetryPolicy(policy);

// add request to the RequestQueue
        queue.add(appListRequest);
    }

    public void requestAppDetails (String url){

        JsonObjectRequest appDetailsRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject jAppDetails = response.optJSONObject("data");

                        Gson gson = new Gson();
                        Type appDetailsType = new TypeToken<AppDetails>(){}.getType();
                        AppDetails appDetails = gson.fromJson(String.valueOf(jAppDetails),
                                appDetailsType);

                        // notify observers, that we have fetched application details
                        EventBus.getDefault().post(new AppDetailsReceivedEvent(appDetails));

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "json download error");
                        EventBus.getDefault().post(new JsonNotFoundEvent());
                    }
                });

        // increase request timeout
        int socketTimeout = 30000;//30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        appDetailsRequest.setRetryPolicy(policy);

    // add request to the RequestQueue
        queue.add(appDetailsRequest);
    }
}
