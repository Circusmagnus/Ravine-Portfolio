package lukasz.wojtach.ravineportfolio.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Łukasz on 2016-01-05.
 */
public class AppDetails implements Serializable {

    public static final String APP_DETAILS_KEY = "APP_DETAILS";

    private String name;
    private String description;
    private String icon;
    private String background;
    private List<String> gallery;
    private List<InternetStore> link;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public List<InternetStore> getLink() {
        return link;
    }

    public void setLink(List<InternetStore> link) {
        this.link = link;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    /*public Bundle toBundle(){
        Bundle bundle = new Bundle();
        bundle.putCharSequence("NAME", name);
        bundle.putCharSequence("DESCRIPTION", description);
        bundle.putCharSequence("ICON", icon);
        bundle.putCharSequence("BACKGROUND", background);
        bundle.putCharSequenceArray("GALERY", galery);

        return bundle;
    }*/
}
