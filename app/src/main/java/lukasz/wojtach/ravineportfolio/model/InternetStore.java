package lukasz.wojtach.ravineportfolio.model;

import java.io.Serializable;

/**
 * Created by Łukasz on 2016-01-05.
 */
public class InternetStore implements Serializable{
    private final String INTERNET_STORE_KEY = "internetStoreKey";
    private String url;
    private String image;

    public String getINTERNET_STORE_KEY() {
        return INTERNET_STORE_KEY;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
