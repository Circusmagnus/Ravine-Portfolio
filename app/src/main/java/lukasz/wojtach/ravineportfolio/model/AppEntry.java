package lukasz.wojtach.ravineportfolio.model;

/**
 * Created by Łukasz on 2016-01-02.
 */
public class AppEntry {
    private String id;
    private String name;
    private String description;
    private String icon;
    private String link;

    public AppEntry(String id, String name, String description, String icon, String link) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public String getLink() {
        return link;
    }
}
