package lukasz.wojtach.ravineportfolio.events;

import java.util.List;

import lukasz.wojtach.ravineportfolio.model.AppEntry;


public class AppListReceivedEvent {
    public final List<AppEntry> appEntries;
    public AppListReceivedEvent(List<AppEntry> appEntries){
        this.appEntries = appEntries;
    }
}
