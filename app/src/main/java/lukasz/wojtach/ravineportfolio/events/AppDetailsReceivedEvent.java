package lukasz.wojtach.ravineportfolio.events;

import lukasz.wojtach.ravineportfolio.model.AppDetails;


public class AppDetailsReceivedEvent {
    public final AppDetails appDetails;
    public AppDetailsReceivedEvent(AppDetails appDetails){
        this.appDetails = appDetails;
    }
}
