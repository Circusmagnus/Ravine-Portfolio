package lukasz.wojtach.ravineportfolio.events;


public class JsonNotFoundEvent {
    public final String message = "Json not found";
}
