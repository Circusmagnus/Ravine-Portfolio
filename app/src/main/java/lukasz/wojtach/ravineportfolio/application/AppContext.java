package lukasz.wojtach.ravineportfolio.application;

import java.util.ArrayList;
import java.util.List;

import lukasz.wojtach.ravineportfolio.model.AppEntry;


public class AppContext {

    private static List<AppEntry> appEntries = new ArrayList<>();

    public static List<AppEntry> getAppEntries() {
        return appEntries;
    }

    public static void setAppEntries(List<AppEntry> appEntries) {
        AppContext.appEntries = appEntries;
    }
}
