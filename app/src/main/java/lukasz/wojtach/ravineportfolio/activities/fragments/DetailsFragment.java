package lukasz.wojtach.ravineportfolio.activities.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.model.AppDetails;
import lukasz.wojtach.ravineportfolio.service.VRequestQue;


public class DetailsFragment extends Fragment {

    private static final String APP_DETAILS_KEY = "APP_DETAILS";

    private AppDetails appDetails;

    private ImageLoader imageLoader;

    @Bind(R.id.app_icon) NetworkImageView appIcon;
    @Bind(R.id.name) TextView appName;
    @Bind(R.id.description) TextView appDescription;
    @Bind(R.id.images_list) LinearLayout galleryLayout;
    @Bind(R.id.online_stores) LinearLayout storesLayout;


    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(Bundle args) {
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            appDetails = (AppDetails)getArguments().getSerializable(APP_DETAILS_KEY);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);

        ButterKnife.bind(this, v);

        // initialize imageLoader
        imageLoader = VRequestQue.getInstance(getActivity().getApplicationContext())
                .getImageLoader();

        // Set header and description
        appIcon.setImageUrl(appDetails.getIcon(), imageLoader);
        appName.setText(appDetails.getName());
        appDescription.setText(appDetails.getDescription());

        // gallery
        for (int i = 0; i < appDetails.getGallery().size(); i++) {
            NetworkImageView networkImageView = new NetworkImageView(getContext());
            initGalleryItem(networkImageView, appDetails.getGallery().get(i), i);
            galleryLayout.addView(networkImageView);
        }

        //Google play, appstore, windows store
        for (int i = 0; i < appDetails.getLink().size(); i++){
            NetworkImageView networkImageView = new NetworkImageView(getContext());
            //networkImageView.setId(i);
            initStoreButton(networkImageView, appDetails.getLink().get(i).getImage(),
                    appDetails.getLink().get(i).getUrl());

            storesLayout.addView(networkImageView);
        }
        return v;
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }



    private void initGalleryItem(NetworkImageView networkImageView, final String imageUrl, int imageId){
        networkImageView.setId(imageId);
        networkImageView.setPadding(6, 6, 6, 6);
        networkImageView.setImageUrl(imageUrl, imageLoader);
        networkImageView.setScaleType(ImageView.ScaleType.FIT_XY);
    }


    private void initStoreButton(NetworkImageView button, final String imageUrl, final String
            storeUrl){
        button.setImageUrl(imageUrl, imageLoader);
        button.setScaleType(NetworkImageView.ScaleType.FIT_XY);
        button.setPadding(6, 6, 6, 6);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
                .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        button.setLayoutParams(params);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(storeUrl));
                startActivity(browserIntent);
            }
        });

    }
}
