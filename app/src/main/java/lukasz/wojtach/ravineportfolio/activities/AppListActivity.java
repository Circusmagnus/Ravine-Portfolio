package lukasz.wojtach.ravineportfolio.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.activities.fragments.AppListFragment;

public class AppListActivity extends NavDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // only this activity may open and close navigation drawer by clicking in the toolbar
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // shows list of applications provided by the api.
        showApps();
    }

    //shows list of applications
    private void showApps(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        AppListFragment appListFragment = AppListFragment.newInstance(1);
        ft.replace(R.id.fragment_container, appListFragment);
        ft.commit();
    }
}
