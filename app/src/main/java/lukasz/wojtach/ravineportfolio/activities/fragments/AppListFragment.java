package lukasz.wojtach.ravineportfolio.activities.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import lukasz.wojtach.ravineportfolio.activities.AppDetailsActivity;
import lukasz.wojtach.ravineportfolio.activities.fragments.adapters.AppListViewAdapter;
import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.application.AppContext;
import lukasz.wojtach.ravineportfolio.model.AppDetails;
import lukasz.wojtach.ravineportfolio.model.AppEntry;
import lukasz.wojtach.ravineportfolio.service.JSONService;
import lukasz.wojtach.ravineportfolio.service.VRequestQue;
import lukasz.wojtach.ravineportfolio.events.AppDetailsReceivedEvent;
import lukasz.wojtach.ravineportfolio.events.AppListReceivedEvent;
import lukasz.wojtach.ravineportfolio.events.JsonNotFoundEvent;

//import android.app.Fragment;

public class AppListFragment extends Fragment implements AppListViewAdapter.OnAppSelected,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = AppListFragment.class.getSimpleName();

    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;

    private JSONService jsonService;


    public AppListFragment() {
    }

    public static AppListFragment newInstance(int columnCount) {
        AppListFragment fragment = new AppListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        RequestQueue queue = VRequestQue.getInstance(getActivity().getApplicationContext())
                .getRequestQueue();
        jsonService = JSONService.getInstance(queue);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_applist_list, container, false);

        ((SwipeRefreshLayout)view).setOnRefreshListener(this);

        // Set the adapter
        setAdapter(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setAdapter(View containerView){
        ImageLoader imageLoader = VRequestQue.getInstance(getActivity().getApplicationContext())
                .getImageLoader();
        RecyclerView recyclerView = (RecyclerView)containerView.findViewById(R.id.list);

        Context context = containerView.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        recyclerView.setAdapter(new AppListViewAdapter(this, imageLoader));
        Log.d(TAG, "new adapter is set");
    }

    @Override
    public void onAppSelected(AppEntry appEntry) {
        // get url to the app details
        String id = appEntry.getId();
        String detailsUrl = getString(R.string.app_details_url) + id;

        // request Json with app details from the url
        jsonService.requestAppDetails(detailsUrl);
    }



    // This method will be called when we receive new List of applications from EventBus
    // happens, when user refreshes the list (pull-to-refresh)
    @Subscribe
    public void onEvent(AppListReceivedEvent appListReceivedEvent){
        Log.i(TAG, "received AppListReceivedEvent");
        AppContext.setAppEntries(appListReceivedEvent.appEntries);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)getActivity()
                .findViewById(R.id.swipeRefreshAppList);
        setAdapter(swipeRefreshLayout);
        swipeRefreshLayout.setRefreshing(false);
    }

    // This method will be called when we receive application details from EventBus
    @Subscribe
    public void onEvent(AppDetailsReceivedEvent appDetailsReceivedEvent){
        Log.i(TAG, "received AppDetailsReceivedEvent");
        AppDetails appDetails = appDetailsReceivedEvent.appDetails;
        Intent showAppDetails = new Intent(getActivity(), AppDetailsActivity.class);
        showAppDetails.putExtra(AppDetails.APP_DETAILS_KEY, appDetails);
        getActivity().startActivity(showAppDetails);
    }

    // When we find no json
    @Subscribe
    public void onEvent(JsonNotFoundEvent jsonNotFoundEvent){
        Log.w(TAG, jsonNotFoundEvent.message);
        //TODO: Dialog window with a message
    }

    // when user pulls screen down to refresh the list of applications
    @Override
    public void onRefresh() {
        jsonService.requestAppList(getString(R.string.app_list_url));
    }
}
