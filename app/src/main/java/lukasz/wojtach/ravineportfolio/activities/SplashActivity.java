package lukasz.wojtach.ravineportfolio.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.RequestQueue;


import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.application.AppContext;
import lukasz.wojtach.ravineportfolio.service.JSONService;
import lukasz.wojtach.ravineportfolio.service.VRequestQue;
import lukasz.wojtach.ravineportfolio.events.AppListReceivedEvent;
import lukasz.wojtach.ravineportfolio.events.JsonNotFoundEvent;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // get an instance of Volley RequestQue
        RequestQueue queue = VRequestQue.getInstance(this.getApplicationContext()).getRequestQueue();

        // url of API with applications list
        String url = getString(R.string.app_list_url);

        // request list of applications
        JSONService jsonService = JSONService.getInstance(queue);
        jsonService.requestAppList(url);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when we receive new List of applications from EventBus
    @Subscribe
    public void onEvent(AppListReceivedEvent appListReceivedEvent){
        Log.i(TAG, "received AppListReceivedEvent");
        AppContext.setAppEntries(appListReceivedEvent.appEntries);
        Intent intent = new Intent(this, AppListActivity.class);
        startActivity(intent);
    }

    // When we find no json
    @Subscribe
    public void onEvent(JsonNotFoundEvent jsonNotFoundEvent){
        Log.w(TAG, jsonNotFoundEvent.message);
        //TODO: Dialog window with a message
        Intent intent = new Intent(this, AppListActivity.class);
        startActivity(intent);
    }
}


