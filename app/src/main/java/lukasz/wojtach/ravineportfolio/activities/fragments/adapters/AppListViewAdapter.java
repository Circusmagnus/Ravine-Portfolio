package lukasz.wojtach.ravineportfolio.activities.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.application.AppContext;
import lukasz.wojtach.ravineportfolio.model.AppEntry;


public class AppListViewAdapter extends RecyclerView.Adapter<AppListViewAdapter.ViewHolder> {

    // holds a List of Apps supplied by the API
    private List<AppEntry> appEntries;

    // whatever object, that is going to react on clicks
    private OnAppSelected onAppSelectedListener;

    // an ImageLoader from our queue
    private ImageLoader imageLoader;

    public AppListViewAdapter(OnAppSelected onAppSelectedListener, ImageLoader imageLoader) {
        this.onAppSelectedListener = onAppSelectedListener;
        appEntries = AppContext.getAppEntries();
        this.imageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.applist_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //requestAppList();
        holder.appEntry = appEntries.get(position);
        holder.appIcon.setImageUrl(appEntries.get(position).getIcon(), imageLoader);
        holder.appName.setText(appEntries.get(position).getName());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onAppSelectedListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    onAppSelectedListener.onAppSelected(holder.appEntry);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
            return appEntries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final NetworkImageView appIcon;
        public final TextView appName;
        public AppEntry appEntry;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            appIcon = (NetworkImageView) view.findViewById(R.id.app_icon);
            appName = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + appName.getText() + "'";
        }
    }

    public interface OnAppSelected {
        void onAppSelected(AppEntry appEntry);
    }
}
