package lukasz.wojtach.ravineportfolio.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.activities.fragments.DetailsFragment;
import lukasz.wojtach.ravineportfolio.model.AppDetails;

public class AppDetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_details);
        // get application details
        AppDetails appDetails = (AppDetails)getIntent().getSerializableExtra(AppDetails.APP_DETAILS_KEY);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)  {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(appDetails.getName());
        }

        showAppDetails(appDetails);
    }

    private void showAppDetails(AppDetails appDetails){
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppDetails.APP_DETAILS_KEY, appDetails);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DetailsFragment detailsFragment = DetailsFragment.newInstance(bundle);
        ft.replace(R.id.fragment_container, detailsFragment);
        ft.commit();
    }

}
