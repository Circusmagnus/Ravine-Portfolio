package lukasz.wojtach.ravineportfolio.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import lukasz.wojtach.ravineportfolio.R;
import lukasz.wojtach.ravineportfolio.activities.fragments.ContactFragment;

public class ContactActivity extends NavDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get a support ActionBar corresponding to this toolbar and enable the Up button
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // initialize fragment
        showContact();
    }

    // shows "Kontakt" screen
    private void showContact(){
        String contactUrl = getString(R.string.contact_url);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ContactFragment contactFragment = ContactFragment.newInstance(contactUrl);
        ft.add(R.id.fragment_container, contactFragment);
        ft.commit();
    }
}
