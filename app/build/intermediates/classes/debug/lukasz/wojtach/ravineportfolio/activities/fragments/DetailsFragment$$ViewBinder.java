// Generated code from Butter Knife. Do not modify!
package lukasz.wojtach.ravineportfolio.activities.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DetailsFragment$$ViewBinder<T extends lukasz.wojtach.ravineportfolio.activities.fragments.DetailsFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492974, "field 'appIcon'");
    target.appIcon = finder.castView(view, 2131492974, "field 'appIcon'");
    view = finder.findRequiredView(source, 2131492986, "field 'appName'");
    target.appName = finder.castView(view, 2131492986, "field 'appName'");
    view = finder.findRequiredView(source, 2131492987, "field 'appDescription'");
    target.appDescription = finder.castView(view, 2131492987, "field 'appDescription'");
    view = finder.findRequiredView(source, 2131492989, "field 'galleryLayout'");
    target.galleryLayout = finder.castView(view, 2131492989, "field 'galleryLayout'");
    view = finder.findRequiredView(source, 2131492990, "field 'storesLayout'");
    target.storesLayout = finder.castView(view, 2131492990, "field 'storesLayout'");
  }

  @Override public void unbind(T target) {
    target.appIcon = null;
    target.appName = null;
    target.appDescription = null;
    target.galleryLayout = null;
    target.storesLayout = null;
  }
}
