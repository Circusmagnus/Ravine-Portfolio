// Generated code from Butter Knife. Do not modify!
package lukasz.wojtach.ravineportfolio.activities.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ContactFragment$$ViewBinder<T extends lukasz.wojtach.ravineportfolio.activities.fragments.ContactFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492985, "field 'webView'");
    target.webView = finder.castView(view, 2131492985, "field 'webView'");
  }

  @Override public void unbind(T target) {
    target.webView = null;
  }
}
