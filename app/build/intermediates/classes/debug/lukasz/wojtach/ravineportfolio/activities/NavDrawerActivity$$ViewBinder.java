// Generated code from Butter Knife. Do not modify!
package lukasz.wojtach.ravineportfolio.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NavDrawerActivity$$ViewBinder<T extends lukasz.wojtach.ravineportfolio.activities.NavDrawerActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492969, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131492969, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131492973, "field 'navigationView'");
    target.navigationView = finder.castView(view, 2131492973, "field 'navigationView'");
    view = finder.findRequiredView(source, 2131492971, "field 'drawer'");
    target.drawer = finder.castView(view, 2131492971, "field 'drawer'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.navigationView = null;
    target.drawer = null;
  }
}
